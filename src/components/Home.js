import React, { Component } from 'react';
import Sidebar from '../components/Sidebar';
import {Col } from 'react-bootstrap';
import HomeTopActionBar from '../components/HomeTopActionBar/HomeTopActionBar';
import Watermark from './Watermark/Watermark';
import ProjectTable from '../components/ProjectTable/ProjectTable';
import {Route,HashRouter} from "react-router-dom";

export default class Home extends Component {

  constructor(props) {
		super(props);
		//console.log(this.props);
		
	}
  render() {
    return (
      <HashRouter>
      <div className="Layout">
       <Col xs={2} className="MainCol">
       <div className="header"> 
       <Sidebar menu="Timer" history={this.props.history}/>
       </div> 
       </Col>
       <Col xs={10} className="MainCol">
        <div>
          <HomeTopActionBar />
        </div>
        <div className="content">
         <Watermark />
         {/* <Route path="/Home" component={Watermark}/>
         <Route path="/ProjectTable" component={ProjectTable}/> */}
        </div>
       </Col>
      </div>
      </HashRouter>
    );
  }
}