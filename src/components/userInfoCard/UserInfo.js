import React, { Component } from 'react';
import './UserInfo.css';

export default class UserInfo extends Component {
  render() {
    return (
        <div className="logoBox1">
         <div className="userAuthorImage">
            <div className="userImage">
              <a href="">
              <img src={this.props.logoImg} className="userAuthor" alt="Company Logo" />
              </a>
            </div>
         </div>
         <div className="userAuthorInfo">
            <p className="userAuthorName">{this.props.userName}</p>
            <p className="userAuthorEmail">{this.props.userMailId}</p>
         </div>
        </div>
    );
  }
}
