import React, { Component } from 'react';
import logo from '../assets/images/fathom.png';
import '../assets/css/Footer.css';

class Footer extends Component {
  render() {
    return (
      <section >
        <div className="footer_container" >
         <img src={logo} className="footer-logo" alt="Company Logo" />
        </div>
      </section>
    );
  }
};
export default Footer;