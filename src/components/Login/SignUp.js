import React, { Component } from 'react';
import classNames from 'classnames';
import validator from 'validator';
import {Row,Col,Button } from 'react-bootstrap';
import './Login.css';
import '../../../src/assets/css/common.css';

class SignUp extends Component {
  	constructor(props) {
		super(props);
		this.state = {
			...this.formDefaults,
			
		};
		
	}
	formDefaults = {
		email: { value: '', isValid: true, message: '' },
		password: { value: '', isValid: true, message: '' },
		confirmPassword: { value: '', isValid: true, message: '' }
	}

  onChange = (e) => {
    const state = {
      ...this.state,
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      }
    };

    this.setState(state);
  }

  onSubmit = (e) => {

    this.props.history.push('Home');
    e.preventDefault();
    // reset states before the validation procedure is run.
    this.resetValidationStates();
    // run the validation, and if it's good move on.
    if (this.formIsValid()) {
        
    }
  }

  formIsValid = () => {
    const email = { ...this.state.email };
    const password = { ...this.state.password };
    const confirmPassword = { ...this.state.confirmPassword };
    let isGood = true;

    if (!validator.isEmpty(email.value)) {
        password.isValid = false;
        password.message = 'Enter email';
        isGood = false;
    }
    else if (!validator.isEmail(email.value)) {
      email.isValid = false;
      email.message = 'Not a valid email address';
      isGood = false;
    }

    if (!validator.isEmpty(password.value)) {
        password.isValid = false;
        password.message = 'Enter Password';
        isGood = false;
    }

    if (!validator.isEmpty(confirmPassword.value)) {
        confirmPassword.isValid = false;
        confirmPassword.message = 'Enter confirm password';
        isGood = false;
    }
    

    // perform addtion validation on password and confirmPassword here...

    if (!isGood) {
      this.setState({
        email,
        password,
        confirmPassword,
      });
    }

    return isGood;
  }

  resetValidationStates = () => {
    // make a copy of everything in state
    const state = JSON.parse(JSON.stringify(this.state));

    /*
    loop through each item in state and if it's safe to assume that only
    form values have an 'isValid' property, we can use that to reset their
    validation states and keep their existing value property. This process
    makes it easy to set all validation states on form inputs in case the number
    of fields on our form grows in the future.
    */
    Object.keys(state).forEach(key => {
      if (state[key].hasOwnProperty('isValid')) {
        state[key].isValid = true;
        state[key].message = '';
      }
    });

    this.setState(state);
  }

  resetForm = () => {
    this.setState(...this.formDefaults);
  }

  render() {
    const { email, password, confirmPassword } = this.state;
    /*
    Each of the group classes below will include the 'form-group' class,
    and will only include the 'has-error' class if the isValid value is false.
    */
    const emailGroupClass = classNames('form-group',
      { 'has-error': !email.isValid }
    );
    const passwordGroupClass = classNames('form-group',
      { 'has-error': !password.isValid }
    );

    const confirmPasswordGroupClass = classNames('form-group',
      { 'has-error': !confirmPassword.isValid }
    );

    return (
            <div>
                <div className="login_container">
                    <form className="form-signin" onSubmit={this.onSubmit}>
                    <Col xs={12} ms={4} >
                      <div className="signin_up_title">
                        <h2 className="form-signin-heading">Sign Up</h2>
                      </div>
                      <div className="signin_up_btn">
                        <a className="signUplink" onClick={this.props.click} >Sign In</a>
                      </div>
                    </Col>
                        <div className={emailGroupClass}>
                        <span className="help-block">{email.message}</span>
                        <input type="text" name="email" className="form-control" placeholder="Email address" value={email.value} onChange={this.onChange} autoFocus />
                        </div>

                        <div className={passwordGroupClass}>
                        <input type="password" name="password" className="form-control" placeholder="Password" value={password.value} onChange={this.onChange} />
                        <span className="help-block">{password.message}</span>
                        </div>

                        <div className={confirmPasswordGroupClass}>
                        <input type="confirmPassword" name="confirmPassword" className="form-control" placeholder="confirmPassword" value={confirmPassword.value} onChange={this.onChange} />
                        <span className="help-block">{confirmPassword.message}</span>
                        </div>

                        <Row className="">
                        <Col xs={12} ms={4} >
                        <Button bsStyle="primary" bsSize="large" type="submit" className="pull-right">Sign Up</Button>
                        </Col>
                        </Row>
                    </form>
                </div>
            </div>
          
    );
  }
};

    export default SignUp;