import React, { Component } from 'react';
import {Row,Col} from 'react-bootstrap';
import './Login.css';
import '../../../src/assets/css/common.css';
import Watermark from '../Watermark/Watermark';
import SignUp from './SignUp';
import SignIn from './SignIn';

class Login extends Component {
	constructor() {
		super();
		this.state = {shown:true};
		this.switchForm = this.switchForm.bind(this)
	}
	switchForm() {
		this.setState({
			shown: !this.state.shown
		});
	}
	render() {
		return (
		<div>
			<Row className="">
				<Col xs={6}>
					<Watermark />
				</Col>
				<Col xs={6}  >
					{this.state.shown?<SignIn click={this.switchForm} history={this.props.history} />:<SignUp click={this.switchForm}/>}
				</Col>
			</Row>
		</div>
		);
	}
};
export default Login;