import React, { Component } from 'react';
import './MenuItems.css';
import {Icon} from 'react-fa';
import {Link} from "react-router-dom";
export default class MenuItems extends Component {
    
    constructor() {
        super(); 
	}
    onMenuChangeHander = (e) => {
        this.props.history.push(this.props.linkToCall);
    };

    render() {
        var status = this.props.status;
        if (this.props.title == this.props.menu) 
        {
            status ='selected';
        } 
          {this.props.title}
        var icon = this.props.icon;
        var linkToCall = this.props.linkToCall;
        return (
            <li className= {"mainListItem " + status} onClick={this.onMenuChangeHander}><span className="listItem"><Icon size='lg' className="menuIcon" name={icon}/><span className="menuText"></span>{this.props.title}</span></li>
        );
    }
}