import React, { Component } from 'react';
import logo from '../../assets/images/fathom.png';
import './Team.css';

class Team extends Component {
  render() {
    return (
      <section >
        <div className="watermark_container" >
         <img src={logo} className="img-responsive" alt="Company Logo" />
        </div>
      </section>
    );
  }
};
export default Team;