import React from 'react';
import ReactTable from 'react-table';
import Sidebar from '../Sidebar';
import HomeTopActionBar from '../../components/HomeTopActionBar/HomeTopActionBar';
import './ProjectTable.css';
import "react-table/react-table.css";
import {Col } from 'react-bootstrap';
class ProjectTable extends React.Component {
	
	render() {
		const data = [{
			name: 'Tanner Linsley',
			age: 1,
			
		  },
		  {
			name: 'Tanner Linsley',
			age: 2,
			
		  },
		  {
			name: 'Tanner Linsley',
			age: 3,
			
		  },
		  {
			name: 'Tanner Linsley',
			age: 4,
		  },
		  {
			name: 'Tanner Linsley',
			age: 5,
		  },
		  {
			name: 'Tanner Linsley',
			age: 6,
		  },
		  {
			name: 'Tanner Linsley',
			age: 7,
		  },
		  {
			name: 'Tanner Linsley',
			age: 8,
		  },
		  {
			name: 'Tanner Linsley',
			age: 9,
		  },
		  {
			name: 'Tanner Linsley',
			age: 10
		  },
		  {
			name: 'Tanner Linsley',
			age: 11,
			
		  },
		  {
			name: 'Tanner Linsley',
			age: 12,
			
		  },
		  {
			name: 'Tanner Linsley',
			age: 13,
		
		  },
		  {
			name: 'Tanner Linsley',
			age: 14,
			
		  }];
		const columns = [{
			Header: 'Name',
			accessor: 'name' // String-based value accessors!
		  }, {
			Header: 'Age',
			accessor: 'age'
		  }];
		
		return (

			<div>
      <div className="Layout">
       <Col xs={2} className="MainCol">
       <div className="header"> 
       <Sidebar menu="Project" history={this.props.history}/>
       </div> 
       </Col>
       <Col xs={10} className="MainCol">
        <div>
          <HomeTopActionBar />
        </div>
        <div className="content">
				<ReactTable
				data={data}
				columns={columns}
			/>
        </div>
       </Col>
      </div>
      </div>
			
		)
	}
}
export default ProjectTable;
