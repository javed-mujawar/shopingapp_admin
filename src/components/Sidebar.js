import React, { Component } from 'react';
import logo from '../assets/images/avator.jpg';
import '../assets/css/sidebar.css';
import UserInfo from '../components/userInfoCard/UserInfo';
import MenuItems from '../components/menuItems/MenuItems';


class Sidebar extends Component {
    constructor() {
        super(); 
        this.state = {
            menus: [
                { title: 'Timer',icon:'clock-o',status:'' ,link:'/Home'},
                { title: 'Dashboard',icon:'dashboard',status:'',link:'/Dashboard' },
                { title: 'Project',icon:'folder',status:'',link:'/ProjectTable' },
                { title: 'Report',icon:'file',status:'',link:'/Report' },
                { title: 'Team',icon:'users',status:'',link:'/Team' },
                { title: 'Log Out',icon:'sign-out',status:'' ,link:'/Log_Out' }
                ]
		};
	}

  render() {
  const menu = this.state.menus.map((data,index) => 
  {
    return (<MenuItems menu={this.props.menu} history={this.props.history} key={index} icon={data.icon} status={data.status}  title={data.title} linkToCall={data.link} />)
  })
    return (
        <section id="sidebar" className="sidebarBx">
            <UserInfo logoImg={logo} userMailId="shivani@gmail.com" userName="Shivani Sonawane"/>
            <div className="inner">
              {menu} 
            </div>
        </section>
    );
  }
}
export default Sidebar;