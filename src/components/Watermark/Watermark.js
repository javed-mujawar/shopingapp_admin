import React, { Component } from 'react';
import logo from '../../assets/images/fathom.png';
import './Watermark.css';

class Watermark extends Component {
  render() {
    return (
      <section >
        <div className="watermark_container" >
         <img src={logo} className="img-responsive" alt="Company Logo" />
        </div>
      </section>
    );
  }
};
export default Watermark;