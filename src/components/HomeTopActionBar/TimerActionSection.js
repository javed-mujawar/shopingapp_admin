import React, { Component } from 'react';
import '../../components/HomeTopActionBar/TimerActionSection.css';
import {Glyphicon,Button } from 'react-bootstrap';

class TimerActionSection extends Component {
  render() {
    return (
      <section className="timerActionSection">
         <div className="timerdiv">
           <span>0:00:00</span>
         </div>
         <div className="btnPlaydiv">
              <Button type="submit" className="btnPlay"><Glyphicon glyph="play" /></Button>
         </div>
      </section>
    );
  }
};
export default TimerActionSection;