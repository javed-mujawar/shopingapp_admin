import React, { Component } from 'react';
import '../../components/HomeTopActionBar/HomeTopActionBar.css';
import {Glyphicon,Button } from 'react-bootstrap';
import TimerActionSection from  './TimerActionSection';;

class HomeTopActionBar extends Component {
  render() {
    return (
      <section >
        <div className="actionbar_container" >
         <div className="createTaskDiv">
           <input type="text" name="text" className="" placeholder="What are you working on?"  />
         </div>
         <div className="createProjetcDiv">
              <Button type="submit" className="btncreateproject"><Glyphicon glyph="plus" /> Create a project</Button>
         </div>
         <div>
           <TimerActionSection />
         </div>

         <div>
           
         </div>
        </div>
      </section>
    );
  }
};
export default HomeTopActionBar;