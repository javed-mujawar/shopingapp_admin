import axios from 'axios';
import qs from 'qs';

const baseUrl = "https://jsonplaceholder.typicode.com/";
class Api {
    request(url, postData) {
        const inst = this;
        return new Promise(function (resolve, reject) {
            var postDataString = qs.stringify(postData);
            var header = inst.getHeader();
            axios.post(baseUrl + url, postDataString, header)
                .then(async (response) => {
                    resolve(response);
                }).catch(function (err) {
                    reject(err);
                });
        });
    }

    doPostRequest(url,postData){ 
        return new Promise(function (resolve, reject) {
            axios.post(baseUrl + url, postData)
                .then(function (response) {
                    if(response.status === 200){
                        resolve(response.data);
                    }else{
                        reject("Error form api")
                    }
                })
                .catch(function (error) {
                    reject( error);
                });
        });
    }
    
    doGetRequest(url){
        return new Promise(function (resolve, reject) {
            axios.get(baseUrl + url)
                .then(function (response) {
                    resolve(response.data);
                })
                .catch(function (error) {
                    reject( error);
                });
        });
    }
   
    saveAppdata(postData) {
        return this.request("Employee/saveApplication", postData);
    }

    checkLogin(postData) {
        return this.request("Employee/check_login", postData);
    }
    getUserList(postData) {
        return this.request("Employee/get_all", postData);
    }

    setToken(token) {
        sessionStorage.setItem('token', token);
    }

    getHeader() {
        const token = sessionStorage.getItem('token') ? sessionStorage.getItem('token') : "";
        const headers = { headers: { Authorization: 'Bearer ' + token, 'content-type': 'multipart/form-data' } }
        return headers
    }
}
export default Api;