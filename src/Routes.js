import React from "react";
import { Router, Route, Switch } from "react-router-dom";

import { createBrowserHistory } from "history";


import Login from './components/Login/Login';
import ProjectTable from './components/ProjectTable/ProjectTable';
import Home from './components/Home';
const history =  createBrowserHistory();
const CustomRoutes = () => (
<Router history={history} >
    <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/Home" component={Home} />
        <Route path="/ProjectTable" component={ProjectTable}/>
        <Route component={() => <div>No such page!</div>} />
    </Switch>
</Router>
)
export default CustomRoutes