import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Customrouter from './Routes';

ReactDOM.render(<Customrouter />, document.getElementById('root'));
registerServiceWorker();
